<?php


namespace nashimoari\ScenarioHandler\States;


class Dummy extends AbstractState
{

    public function run(): string
    {

        if (isset($this->params['resultStatus'])) {
            $this->context->setFinishStatus((bool)$this->params['resultStatus']);
        }

        return (string)$this->node->nextNode;
    }
}

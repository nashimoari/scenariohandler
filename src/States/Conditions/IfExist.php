<?php


namespace Nashimoari\ScenarioHandler\States;


use Nashimoari\ScenarioHandler\States\AbstractState;

class IfExist extends AbstractState
{

    public function run()
    {
        $nextNode = (string)$this->node->resultlist[0]->false;
        $this->logIt('params',$this->params);

        if (strlen($this->params['check'])>0) {
            $nextNode = (string)$this->node->resultlist[0]->true;
        }

        return $nextNode;

    }
}

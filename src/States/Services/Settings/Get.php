<?php


namespace Nashimoari\ScenarioHandler\States\Services\Settings;


use Nashimoari\ScenarioHandler\States\AbstractState;

class Get extends AbstractState
{

    public function run():string
    {
        $nextNode = (string)$this->node->resultlist[0]->false;

        $id = $this->params['id'];
        $code = $this->params['code'];

        $settings = new Settings();

        $data = [];
        try {
            $val = $settings->get($code, $id);
            $data = json_decode($val, true);
            $nextNode = (string)$this->node->resultlist[0]->true;
        } catch (\Exception $e) {

        }

        $this->context->setParam($this->params['saveTo'], $data);
        $this->logIt($this->params['saveTo'], $data);

        return $nextNode;
    }
}

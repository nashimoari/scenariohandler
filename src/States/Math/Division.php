<?php


namespace Nashimoari\ScenarioHandler\States\Math;


use Nashimoari\ScenarioHandler\States\AbstractState;


class Division extends AbstractState
{

    public function run(): string
    {
        try {
            $nextNode = 'true';

            $dividend = (int)$this->params['dividend'];
            $this->logIt('dividend', $dividend);

            $divider = (int)$this->params['divider'];
            $this->logIt('divider', $divider);

            $result = $dividend / $divider;
            $this->logIt('result', $result);

            $this->context->setParam($this->params['saveTo'], $result);
        } catch (\Throwable $e) {
            $nextNode = 'false';
        }

        return (string)$this->node->resultlist[0]->$nextNode;
    }
}

<?php


namespace Nashimoari\ScenarioHandler\States\Services\Laravel;

use Nashimoari\ScenarioHandler\States\AbstractState;
use Nashimoari\ScenarioHandler\Helpers\Utils;
use Illuminate\Support\Facades\Queue;

class PushReqToQueue extends AbstractState
{

    public function run(): string
    {
        $nextNode = 'false';

        $this->logIt('params', $this->params);

        $className = $this->params['className'];
        $queueName = $this->params['queueName'];

        try {
            $fields = [];

            foreach ($this->node->parameterslist[0] as $index => $item) {

                $currentVal = $this->params[$index];
                if (isset($item[@jsonToArr])) {
                    $currentVal = json_decode($currentVal, 1);
                }

                if (isset($item[@fromFile])) {
                    $filePath = (string)$item[@fromFile];
                    $currentVal = file_get_contents(storage_path() . "/{$filePath}");

                    // Прогоняем текст для подстановки параметров
                    $currentVal = Utils::paramsReplacer($this->variables, $currentVal);
                }


                if (isset($item[@toRequest])) {
                    $fields[$index] = $currentVal;
                }
            }
            $this->logIt('fields', $fields);

            $class = new $className($fields);

            Queue::pushOn($queueName, $class);
            $nextNode = 'true';
        } catch (\Throwable $e) {
            $this->logIt('errorMessage', $e->getMessage());
            $this->logIt('errorTrace', $e->getTraceAsString());
        }

        return (string)$this->node->resultlist[0]->$nextNode;
    }
}

<?php


namespace App\Services\ScenarioProcessor\States\TinkoffMerchantAPI;


use App\Services\MerchantAPI\TinkoffMerchantAPI;
use Nashimoari\ScenarioHandler\States\AbstractState;
use Nashimoari\Settings;

final class InitPayment extends AbstractState
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run() :string
    {
        $result = 'false';

        $settings = new Settings();
        $tinkoffCredentials = json_decode($settings->get('MerchantAPI', 'TinkoffTEST'), true);

        $merchant = new TinkoffMerchantAPI($tinkoffCredentials['terminalKey'], $tinkoffCredentials['secretKey']);

        $paymentData = [
            'Amount' => $paySum * 100, // Сумма в копейках
            'OrderId' => $request_id, // Идентификатор заказа в системе продавца
            'IP' => $data['clientIP'], // IP-адрес покупателя
            'Language' => 'ru',
            'SuccessURL' => (request()->root()) . $tinkoffCredentials['SuccessURLPath'],
            'Description' => "Заказ игры онлайн"
        ];

        if (isset($tinkoffCredentials['NotificationURLPath'])) {
            $paymentData['NotificationURL'] = (request()->root()) . $tinkoffCredentials['NotificationURLPath'];
        }

        $this->saveRequestData($request_id, 'paymentRequestData', json_encode($paymentData));

        $paymentResponseJSON = $merchant->init($paymentData);

    }
}

<?php


namespace Nashimoari\ScenarioHandler\States;


class Sleep extends AbstractState
{
    public function run()
    {
        $sleep = $this->params['sleep'];
        $this->logIt('sleep_from',time());
        sleep($sleep);
        $this->logIt('sleep_to',time());
        return (string)$this->node->resultlist->nextNode;
    }

}

<?php


namespace Nashimoari\ScenarioHandler\States;


/**
 * Class Compute
 * @package nashimoari\ScenarioHandler\States
 */
class ModuloComparision extends AbstractState
{

    public function run(): string
    {
        $nextNode = 'false';
        $var1 = (int)$this->params['var1'];
        $this->logIt('var1', $var1);

        $var2 = (int)$this->params['var2'];
        $this->logIt('var2', $var2);

        $computeType = $this->node->parameterslist[0]->computeResult[@type];

        $result = $this->params['computeResult'];;

        switch ($computeType) {
            case 'absLowerThen':
                if (abs($var1) < abs($result)) {
                    $nextNode = 'true';
                }
                break;
        }

        $this->logIt('nextNode', $nextNode);
        return (string)$this->node->resultlist[0]->$nextNode;
    }
}

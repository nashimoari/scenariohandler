<?php


namespace Nashimoari\ScenarioHandler\States\DateTime;

use Nashimoari\ScenarioHandler\States\AbstractState;

use DateTimeZone;



class DateFormatter extends AbstractState
{
    private $arrDays = [
        'RU' => ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
        'EN' => ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
    ];


    public function run()
    {
        /**
         * Получаем день недели
         *
         */

        $nextNode = 'false';
        try {

            $date = $this->params['date'];
            $time = $this->params['time'];

            /**
             * Таймзона указанной даты/времени
             */
            $timeZone = (string)$this->params['timeZone'];
            $timeZone = $this->prepareTimeZone($timeZone);
            $this->logIt('preparedTimeZone', $timeZone);

            /**
             * Тайм зона в которое нужно перевести
             */
            $toTimeZone = (string)$this->params['toTimeZone'];
            $toTimeZone = $this->prepareTimeZone($toTimeZone);
            $this->logIt('preparedToTimeZone', $toTimeZone);


            $dateTime = \DateTime::createFromFormat('Y-m-d G:i', $date . " " . $time, new DateTimeZone($timeZone));
            if ($dateTime === false) {
                $this->logIt('createFromFormat', 'error');
                throw new \Exception('DateTime\DateTimePrepare: createFromFormat error');
            }

            $dateTime->setTimezone(new DateTimeZone($toTimeZone));
            $formattedDT = $dateTime->format($this->params['format']);
            $formattedDT = $this->weekDayToRUS($formattedDT);
            $this->logIt('formattedDT', $formattedDT);

            $this->context->setParam($this->params['saveTo'], $formattedDT);
            $nextNode = 'true';
        } catch (\Throwable $e) {
            Log::debug($e->getMessage());
            $nextNode = 'false';
        }

        return (string)$this->node->resultlist[0]->$nextNode;
    }


    private function weekDayToRUS($text)
    {
        return str_replace($this->arrDays['EN'], $this->arrDays['RU'], $text);
    }

    private function prepareTimeZone($tzHour)
    {
        /**
         * Проверяем что у нас не отрицательное значение, так как если положительное, то нам нужно добавить +
         *
         */
        if ($tzHour < 0) {
            if (strlen($tzHour) == 2) {
                $timeZone = '-0' . substr($tzHour, 1, 1);
            }
        } else {
            if (strlen($tzHour) == 1) {
                $timeZone = '+0' . $tzHour;
            } else {
                $timeZone = '+' . $tzHour;
            }
        }

        /**
         * Добавляем в конец два нуля чтобы получилось корректное значение смещения часового пояса
         */
        $timeZone = $timeZone . '00';
        return $timeZone;
    }
}

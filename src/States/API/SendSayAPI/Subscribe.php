<?php


namespace App\Services\ScenarioProcessor\States\SendSay;


use App\Services\ScenarioProcessor\States\AbstractState;
use App\Services\SendSay\SendSay;

final class Subscribe extends AbstractState
{
    public function run(): string
    {
        try {
            $nextNode = 'false';

            $sendSay = new SendSay();

            $url = $this->params['url'];
            $request = json_decode($this->params['data'],true);
            $this->logIt('request', $request);

            $result = $sendSay->sendRequest($request, $url);
            $this->logIt('result', $result);

            if (isset($result['errors']) && sizeof($result['errors'])>0) {
                throw new \Exception('SendSay API return errors');
            }

            $this->context->setParam($this->params['saveTo'], $result);
            $nextNode = 'true';
        } catch (\Throwable $e) {
            $this->logIt('error', $e->getMessage());
        }

        return (string)$this->node->resultlist[0]->$nextNode;
    }
}

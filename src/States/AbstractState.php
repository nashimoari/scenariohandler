<?php


namespace Nashimoari\ScenarioHandler\States;


use Nashimoari\ScenarioHandler\Context;
use Nashimoari\ScenarioHandler\Helpers\Utils;


abstract class AbstractState
{
    /**
     * @var Context
     */
    protected $context;
    protected $node;

    protected $variables = [];
    protected $params = [];

    private $log = [];

    public function __construct(Context $context, \SimpleXMLElement $node)
    {
        $this->context = $context;
        $this->node = $node;

        $this->parseVariables();
        $this->prepareParams();
    }

    public abstract function run(): string;

    public function getLog()
    {
        return $this->log;
    }


    /**
     * @return void
     */
    protected function parseVariables()
    {
        if (!isset($this->node->variables[0])) {
            return;
        }

        $variables = $this->node->variables[0];

        foreach ($variables as $item) {
            $variableName = $item->getName();

            $src = (string)$item[@source];

            $srcArr = [];

            if ($src == 'request') {
                $srcArr = $this->context->getRequest();
                $this->variables[$variableName] = Utils::getData($srcArr, (string)$item);
            }

            if ($src == 'contextParam') {
                $srcArr = $this->context->getParams();
                $this->variables[$variableName] = Utils::getData($srcArr, (string)$item);
            }

            if ($src == 'system') {
                switch ((string)$item) {
                    case 'currentTimeStamp':
                        $this->variables[$variableName] = time();
                        break;
                    case 'date':
                        $this->variables[$variableName] = date((string)$item[@format]);
                        break;
                }
            }

            /**
             * Постобработка если есть параметры для выделения значения из полученного текста
             */
            $delimiter = (string)$item[@delimiter];

            if (!empty($delimiter)) {
                $position = (string)$item[@position];
                $tmpArr = explode($delimiter, $this->variables[$variableName]);
                $this->variables[$variableName] = $tmpArr[$position];
            }

            /**
             * Выделение текста по регулярному выражению в параметр
             */
            $regexPattern = (string)$item[@regexp];
            if (!empty($regexPattern)) {
                if (preg_match($regexPattern, $this->variables[$variableName], $matches)) {
                    $this->variables[$variableName] = $matches['param'];
                }
            }

        }
        $this->logIt('variables', $this->variables);
    }

    /**
     * @return void
     */
    protected function prepareParams()
    {
        $parametersList = $this->node->parameterslist[0];

        foreach ($parametersList as $item) {
            $paramName = $item->getName();
            $this->params[$paramName] = Utils::paramsReplacer($this->variables, (string)$item);
        }
    }


    protected function logIt($name, $val)
    {
        if ($this->context->getDebugMode()) {
            $this->log[$name] = $val;
        }
    }

}

<?php


namespace Nashimoari\ScenarioHandler\Helpers;


final class Utils
{

    /**
     * @param array $arr Input Array
     * @param string $path Input path to search value
     * @return mixed|string
     */
    static public function getData($arr, $path)
    {
        $out_res = '';
        $param = $arr;
        $path_arr = explode('/', $path);
        for ($i = 0; $i < sizeof($path_arr); $i++) {
            if (isset($param[$path_arr[$i]])) {
                $param = $param[$path_arr[$i]];
            } else break;
        }
        if ($i == sizeof($path_arr)) {
            $out_res = $param;
        }
        return $out_res;
    }

    /**
     * @param array $rp Input array with replaced values
     * @param string $string Input string for replace variables
     * @return string
     */
    static public function paramsReplacer(array $rp, $string)
    {
        if (!is_array($rp)) {
            return $string;
        };
        // проходимся по массиву, если находим название параметра обрамленное "%"
        // заменяем его на значение параметра
        $keys = array_keys($rp);
        foreach ($keys as $key) {
            if (!is_array($rp[$key])) {
                $string = str_replace('%' . $key . '%', $rp[$key], $string);
            }
        }

        return $string;
    }

    static function getToken()
    {
        $better_token = md5(uniqid(rand(), 1));
        return $better_token;
    }


}

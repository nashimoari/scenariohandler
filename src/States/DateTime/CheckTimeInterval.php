<?php


namespace Nashimoari\ScenarioHandler\States;

use DateTime;
use DateTimeZone;

class CheckTimeInterval extends AbstractState
{

    public function run(): string
    {
        try {
            $nextNode = 'false';

            $fromHour = $this->params['fromHour'];
            $toHour = $this->params['toHour'];
            $timeZone = $this->params['timeZone'];

            if (isset($this->params['currentTime'])) {
                $currentTime = new DateTime($this->params['currentTime'], new DateTimeZone($timeZone));
            } else {
                // get current time based on timezone
                $currentTime = new DateTime("now", new DateTimeZone($timeZone));
            }

            $hour = (int)$currentTime->format('G');

            if ($hour >= $fromHour && $hour < $toHour) {
                $nextNode = 'true';
            }
        } catch (\Throwable $e) {
            $this->logIt();
        }

        $this->logIt('nextNode', $nextNode);

        return (string)$this->node->resultlist[0]->$nextNode;
    }
}

<?php


namespace Nashimoari\ScenarioHandler\States;


class IfCondition extends AbstractState
{

    public function run()
    {
        $nextNode = (string)$this->node->resultlist[0]->false;

        if ($this->params['param1'] == $this->params['param2']) {
            $nextNode = (string)$this->node->resultlist[0]->true;
        }

        return $nextNode;
    }
}

<?php


namespace Nashimoari\ScenarioHandler\States\DateTime;


use Nashimoari\ScenarioHandler\States\AbstractState;
use DateTimeZone;


class DatePrepare extends AbstractState
{


    public function run()
    {
        /**
         * Форматируем дату с учетом часового пояса
         *
         * Формат входящей даты
         */

        $nextNode = 'false';
        try {
            $date = $this->params['date'];
            $time = $this->params['time'];
            $timeZone = (string)$this->params['timeZone'];

            /**
             * Проверяем что у нас не отрицательное значение, так как если положительное, то нам нужно добавить +
             *
             */
            if ($timeZone<0) {
                if (strlen($timeZone)==2) {
                    $timeZone = '-0'.substr($timeZone,1,1);
                }
            } else {
                if (strlen($timeZone)==1) {
                    $timeZone = '+0'.$timeZone;
                } else {
                    $timeZone = '+'.$timeZone;
                }
            }

            /**
             * Добавляем в конец два нуля чтобы получилось корректное значение смещения часового пояса
             */
            $timeZone = $timeZone.'00';
            $this->logIt('preparedTimeZone',$timeZone);


            $dateTime = \DateTime::createFromFormat('Y-m-d G:i', $date . " " . $time, new DateTimeZone($timeZone));
            if ($dateTime === false) {
                throw new \Exception('DateTime\DatePrepare: createFromFormat error');
            }

            $formattedDT = $dateTime->format('d.m.Y');
            $this->logIt('formattedDT',$formattedDT);

            $this->context->setParam($this->params['saveTo'], $formattedDT);
            $nextNode = 'true';
        } catch (\Throwable $e) {
            Log::debug($e->getMessage());
            $nextNode = 'false';
        }

        return (string)$this->node->resultlist[0]->$nextNode;
    }
}

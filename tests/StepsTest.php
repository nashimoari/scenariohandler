<?php

namespace Nashimoari\ScenarioHandler;

use PHPUnit\Framework\TestCase;

class StepsTest extends TestCase
{

    public function testCheckTimeInterval()
    {
        $scenario = '<scenarios>
    <automat>
        <node name="Test" object="CheckTimeInterval">
            <variables>
                <inputMessage source="request">data/PARAMS/MESSAGE</inputMessage>
            </variables>
            <parameterslist>
            <currentTime>%currentTime%</currentTime>
                <fromHour>10</fromHour>
                <toHour>22</toHour>
                <timeZone>Europe/Kaliningrad</timeZone>
            </parameterslist>
            <resultlist>
                <true/>
                <false/>
            </resultlist>
        </node>
        </automat>
        </scenarios>';

        $tests = [];
        $tests[] = ['currentTime' => '2000-01-01 13:00', 'result' => 'true'];
        $tests[] = ['currentTime' => '2000-01-01 09:00', 'result' => 'false'];
        $tests[] = ['currentTime' => '2000-01-01 22:00', 'result' => 'false'];
        $tests[] = ['currentTime' => '2000-01-01 21:59', 'result' => 'true'];
        $tests[] = ['currentTime' => '2000-01-01 22:30', 'result' => 'false'];

        foreach ($tests as $item) {
            $testScenario = str_replace('%currentTime%', $item['currentTime'], $scenario);

            $scenarioXML = simplexml_load_string($testScenario);
            $context = new Context($this, 1);
            $context->setDebugModeOn();
            $context->setRequest([]);
            $context->runXML($scenarioXML);
            $debugData = $context->getArrayLog();
            unset ($context);
            $this->assertEquals($item['result'], $debugData['automat'][0]['data']['nextNode']);
        }

    }

    public function testRegExVariableExtract()
    {
        $scenario = '<scenarios>
    <automat>
        <node name="Test" object="Dummy">
            <variables>
                <test source="request" regexp="/\|(?P&lt;param&gt;\d*?)#widget\|/">test</test>
            </variables>
            <parameterslist>
                <test>%test%</test>
            </parameterslist>
            <nextNode/>
        </node>
        </automat>
        </scenarios>';

        $scenarioXML = simplexml_load_string($scenario);
        $context = new Context($this, 1);
        $context->setDebugModeOn();
        $context->setRequest(['test' => 'c2d_1gt_connector|56456|7777777#widget|56345643']);
        $context->runXML($scenarioXML);
        $debugData = $context->getArrayLog();
        unset ($context);

        $this->assertEquals('7777777', $debugData['automat'][0]['data']['variables']['test']);
    }

    public function testMessageCheck()
    {
        $scenario = '<scenarios>
    <automat><node name="ChooseCommand" object="MessageCheck">
            <variables>
                <inputMessage source="request">test</inputMessage>
            </variables>
            <parameterslist>
                <message>%inputMessage%</message>
            </parameterslist>
            <nextNodeCondition>
                <condition value="hello" CompareType="FullMatch">Test_1</condition>
                <condition value="list" CompareType="FullMatch">Test_2</condition>
                <condition value="123" CompareType="FullMatch">Test_3</condition>
                <condition value="Проверка инклюзии" CompareType="Inclusion">Test_4</condition>
            </nextNodeCondition>
            <defaultNextNode>Test_default</defaultNextNode>
        </node>
        <node name="Test_1" object="Dummy"><variables/><parameterslist/><nextNode/></node>
        <node name="Test_2" object="Dummy"><variables/><parameterslist/><nextNode/></node>
        <node name="Test_3" object="Dummy"><variables/><parameterslist/><nextNode/></node>
        <node name="Test_4" object="Dummy"><variables/><parameterslist/><nextNode/></node>
        <node name="Test_default" object="Dummy"><variables/><parameterslist/><nextNode/></node>
        </automat>
        </scenarios>';


        $tests = [];
        $tests[] = ['test' => 'hello', 'result' => 'Test_1'];
        $tests[] = ['test' => 'list', 'result' => 'Test_2'];
        $tests[] = ['test' => '123', 'result' => 'Test_3'];
        $tests[] = ['test' => 'Проверка инклюзии', 'result' => 'Test_4'];
        $tests[] = ['test' => '_+_+_+Проверка инклюзии', 'result' => 'Test_4'];
        $tests[] = ['test' => 'Проверка инклюзии+_+_+_+', 'result' => 'Test_4'];
        $tests[] = ['test' => '+_+_++_+Проверка инклюзии+_+_+_+', 'result' => 'Test_4'];
        $tests[] = ['test' => 'Текст которого нет в списке кондишнов. По нему уходим на дефолтную ноду', 'result' => 'Test_default'];

        foreach ($tests as $item) {
            $scenarioXML = simplexml_load_string($scenario);
            $context = new Context($this, 1);
            $context->setDebugModeOn();
            $context->setRequest(['test' => $item['test']]);
            $context->runXML($scenarioXML);
            $debugData = $context->getArrayLog();
            unset ($context);
            $this->assertEquals($item['result'], $debugData['automat'][0]['nextNodeName']);
        }
    }

    public function testUnixTimeStampVariable()
    {
        $scenario = '<scenarios>
    <automat>
        <node name="Test" object="Dummy">
            <variables>
                <test source="system">currentTimeStamp</test>
            </variables>
            <parameterslist>
                <test>%test%</test>
            </parameterslist>
            <nextNode/>
        </node>
        </automat>
        </scenarios>';

        $scenarioXML = simplexml_load_string($scenario);
        $context = new Context($this, 1);
        $context->setDebugModeOn();
        $context->setRequest([]);
        $context->runXML($scenarioXML);
        $debugData = $context->getArrayLog();
        unset ($context);

        $this->assertLessThan(1, time() - $debugData['automat'][0]['data']['variables']['test']);
    }

    public function testCompute()
    {
        $scenario = '<scenarios>
    <automat>
                <node name="Test" object="Compute">
            <variables>
                <var1 source="request">var1</var1>
                <var2 source="request">var2</var2>
                <res source="request">result</res>
            </variables>
            <parameterslist>
                <var1>%var1%</var1>
                <var2>%var2%</var2>
                <computeResult type="absLowerThen">%res%</computeResult>
            </parameterslist>
            <resultlist>
                <true/>
                <false/>
            </resultlist>
        </node>
        </automat>
        </scenarios>';

        $tests = [];
        $tests[] = ['request' => ['var1' => 10, 'var2' => 10, 'result' => 1], 'result' => 'true'];
        $tests[] = ['request' => ['var1' => 20, 'var2' => 10, 'result' => 11], 'result' => 'true'];
        $tests[] = ['request' => ['var1' => 10, 'var2' => 20, 'result' => 11], 'result' => 'true'];
        $tests[] = ['request' => ['var1' => 20, 'var2' => 10, 'result' => 10], 'result' => 'false'];
        $tests[] = ['request' => ['var1' => 10, 'var2' => -10, 'result' => 19], 'result' => 'false'];
        $tests[] = ['request' => ['var1' => -10, 'var2' => 10, 'result' => 19], 'result' => 'false'];
        $tests[] = ['request' => ['var1' => 10, 'result' => 1], 'result' => 'false'];
        $tests[] = ['request' => ['var1' => 10, 'result' => 20], 'result' => 'true'];


        foreach ($tests as $item) {
            $scenarioXML = simplexml_load_string($scenario);
            $context = new Context($this, 1);
            $context->setDebugModeOn();
            $context->setRequest($item['request']);
            $context->runXML($scenarioXML);
            $debugData = $context->getArrayLog();
            unset ($context);
            $this->assertEquals($item['result'], $debugData['automat'][0]['data']['nextNode']);
        }
    }

    public function testMemcachedSet()
    {
        $scenario = '<scenarios>
    <variables>
        <memcachedServer>10.10.10.17</memcachedServer>
        <memcachedPort>11211</memcachedPort>
    </variables>
    <automat>
                <node name="Test" object="Memcached\Set">
            <variables/>
            <parameterslist>
                <Key>testMemcached</Key>
                <Val>test value</Val>
            </parameterslist>
            <resultlist>
                <true/>
                <false/>
            </resultlist>
        </node>
        </automat>
        </scenarios>';

        $scenarioXML = simplexml_load_string($scenario);
        $context = new Context($this, 0);
        $context->setDebugModeOn();
        $context->setRequest([]);
        $context->runXML($scenarioXML);
        $debugData = $context->getArrayLog();
        print_r($debugData);
    }

    public function testMemcachedSetWithTTL()
    {
        $scenario = '<scenarios>
    <variables>
        <memcachedServer>10.10.10.17</memcachedServer>
        <memcachedPort>11211</memcachedPort>
    </variables>
    <automat>
                <node name="Test" object="Memcached\Set">
            <variables/>
            <parameterslist>
                <Key>testMemcached</Key>
                <Val>test value</Val>
                <TTL>3</TTL>
            </parameterslist>
            <resultlist>
                <true/>
                <false/>
            </resultlist>
        </node>
        </automat>
        </scenarios>';

        $scenarioXML = simplexml_load_string($scenario);
        $context = new Context($this, 0);
        $context->setDebugModeOn();
        $context->setRequest([]);
        $context->runXML($scenarioXML);
        $debugData = $context->getArrayLog();
        print_r($debugData);
    }

    public function testMemcachedGet()
    {
        $scenario = '<scenarios>
    <variables>
        <memcachedServer>10.10.10.17</memcachedServer>
        <memcachedPort>11211</memcachedPort>
    </variables>
    <automat>
                <node name="Test" object="Memcached\Get">
            <variables/>
            <parameterslist>
                <Key>testMemcached</Key>
                <saveToParam>testVal</saveToParam>
            </parameterslist>
            <resultlist>
                <true/>
                <false/>
            </resultlist>
        </node>
        </automat>
        </scenarios>';

        $scenarioXML = simplexml_load_string($scenario);
        $context = new Context($this, 0);
        $context->setDebugModeOn();
        $context->setRequest([]);
        $context->runXML($scenarioXML);
        $debugData = $context->getArrayLog();
        print_r($debugData);
//            unset ($context);
//            $this->assertEquals($item['result'], $debugData['automat'][0]['data']['nextNode']);
    }

    public function testMemcachedDelete()
    {
        $scenario = '<scenarios>
    <variables>
        <memcachedServer>10.10.10.17</memcachedServer>
        <memcachedPort>11211</memcachedPort>
    </variables>
    <automat>
      <node name="Test1" object="Memcached\Set">
            <variables/>
            <parameterslist>
                <Key>testMemcached</Key>
                <Val>test value</Val>
            </parameterslist>
            <resultlist>
                <true>Test2</true>
                <false/>
            </resultlist>
      </node>
      
      <node name="Test2" object="Memcached\Get">
            <variables/>
            <parameterslist>
                <Key>testMemcached</Key>
                <saveToParam>testVal</saveToParam>
            </parameterslist>
            <resultlist>
                <true>Test3</true>
                <false/>
            </resultlist>
        </node>

      <node name="Test3" object="Memcached\Delete">
            <variables/>
            <parameterslist>
                <Key>testMemcached</Key>
            </parameterslist>
            <resultlist>
                <true>Test4</true>
                <false/>
            </resultlist>
        </node>
        
        <node name="Test4" object="Memcached\Get">
            <variables/>
            <parameterslist>
                <Key>testMemcached</Key>
                <saveToParam>testVal2</saveToParam>
            </parameterslist>
            <resultlist>
                <true/>
                <false/>
            </resultlist>
        </node>
        </automat>
        </scenarios>';

        $scenarioXML = simplexml_load_string($scenario);
        $context = new Context($this, 0);
        $context->setDebugModeOn();
        $context->setRequest([]);
        $context->runXML($scenarioXML);
        $debugData = $context->getArrayLog();
        print_r($debugData);
//            unset ($context);
//            $this->assertEquals($item['result'], $debugData['automat'][0]['data']['nextNode']);
    }

    public function testCycle()
    {
        $scenario = '<scenarios>
    <variables>
      <maxRetryNodeCounter>3</maxRetryNodeCounter>    
    </variables>
    <automat>
        <node name="Test1" object="Dummy">
            <variables>
                <test source="system">currentTimeStamp</test>
            </variables>
            <parameterslist>
                <test>%test%</test>
            </parameterslist>
            <nextNode>Test1</nextNode>
        </node>
        </automat>
        </scenarios>';

        $scenarioXML = simplexml_load_string($scenario);
        $context = new Context($this, 0);
        $context->setDebugModeOn();
        $context->setRequest([]);
        $context->runXML($scenarioXML);
        $debugData = $context->getArrayLog();
        //print_r($debugData);
//            unset ($context);
        $this->assertEquals(4, count($debugData['automat']));
    }

    public function testDateFormatter()
    {
        $scenario = '<scenarios>
    <variables>
      <maxRetryNodeCounter>3</maxRetryNodeCounter>    
    </variables>
    <automat>
    <node name="prepareDateForEmail" object="DateTime\DateFormatter">
            <about>Получаем день недели</about>
            <variables>
                <Date source="request">date</Date>
                <Time source="request">time_begin</Time>
                <TimeZone source="request">timeZone</TimeZone>
            </variables>
            <parameterslist>
                <date>%Date%</date>
                <time>%Time%</time>
                <timeZone>3</timeZone>
                <toTimeZone>%TimeZone%</toTimeZone>
                <format>d.m.Y, l, H:i (\U\T\C P)</format>
                <saveTo>DateForEmail</saveTo>
            </parameterslist>
            <resultlist>
                <true></true>
                <false>fail</false>
            </resultlist>
        </node>
        
        <node name="fail" object="Dummy">
        <variables/>
          <parameterslist>
            <resultStatus>0</resultStatus>
          </parameterslist>
            <nextNode/>
        </node>
        
        </automat>
        </scenarios>';

        $scenarioXML = simplexml_load_string($scenario);
        $context = new Context($this, 0);
        $context->setDebugModeOn();
        $context->setRequest(['date' => '2000-01-21', 'time_begin' => '19:00', 'timeZone' => '10']);
        $context->runXML($scenarioXML);
        $debugData = $context->getArrayLog();
        print_r($debugData);
        //unset ($context);
        $this->assertEquals('22.01.2000, Суббота, 02:00 (UTC +10:00)', $debugData['automat'][0]['data']['formattedDT']);
    }

    public function testMathDivision()
    {
        $scenario = '<scenarios>
    <variables>
      <maxRetryNodeCounter>3</maxRetryNodeCounter>    
    </variables>
    <automat>
        <node name="Test1" object="Math\Division">
            <variables>
            </variables>
            <parameterslist>
                <dividend>10</dividend>
                <divider>5</divider>
                <saveTo>result</saveTo>
            </parameterslist>
            <resultlist>
                <true></true>
                <false>fail</false>
            </resultlist>
        </node>
        </automat>
        </scenarios>';

        $scenarioXML = simplexml_load_string($scenario);
        $context = new Context($this, 0);
        $context->setDebugModeOn();
        $context->setRequest([]);
        $context->runXML($scenarioXML);
        $debugData = $context->getArrayLog();
        // print_r($debugData);
//            unset ($context);
        $this->assertEquals(2, $debugData['automat'][0]['data']['result']);
    }


}

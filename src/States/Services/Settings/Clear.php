<?php


namespace Nashimoari\ScenarioHandler\States\Services\Settings;


use Nashimoari\ScenarioHandler\States\AbstractState;
use Nashimoari\Settings;


class Clear extends AbstractState
{

    public function run(): string
    {
        $id = $this->params['id'];
        $code = $this->params['code'];

        $settings = new Settings();
        $settings->update($code, "$id", json_encode([]));

        return (string)$this->node->nextNode;
    }
}

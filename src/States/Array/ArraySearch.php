<?php


namespace Nashimoari\ScenarioHandler\States;


class ArraySearch extends AbstractState
{
    private $variables;

    public function run(): string
    {
        $srcArrName = (string)$this->node->parameterslist[0]->sourceArray;
        $saveToParam = (string)$this->node->parameterslist[0]->saveToParam;
        $arrayParamName = (string)$this->node->parameterslist[0]->arrayParamName;

        $needle = (string)$this->node->parameterslist[0]->needle;
        $searchType = (string)$this->node->parameterslist[0]->needle[@searchType];

        $srcArr = $this->context->getParam($srcArrName);

        $this->variables = $this->parseVariables($this->node->variables[0]);

        $bagOfWord = explode(' ', $needle);

        foreach ($srcArr as $item) {
            if (!isset($item[$arrayParamName])) {
                continue;
            }

            if ($searchType == 'phrase') {

            }

            if ($searchType == 'word') {

                foreach ($bagOfWord as $word) {

                }
            }

        }


        $this->context->setParam($saveToParam, $result);


        return (string)$this->node->nextNode;
    }
}

<?php


namespace Nashimoari\ScenarioHandler\States\Memcached;

use Nashimoari\ScenarioHandler\States\AbstractState;
use Exception;

final class Get extends AbstractState
{
    /**
     * @return string
     * @throws Exception
     */
    public function run():string
    {
        $result = 'false';
        $testMode = $this->context->getTestMode();
        $key = $this->params['Key'];

        $server = $this->context->getGlobalVariable('memcachedServer');
        $port = $this->context->getGlobalVariable('memcachedPort');


        $this->logIt('key', $key);
        $this->logIt('server', $server);
        $this->logIt('port', $port);

        /**
         * Check test mode
         */
        if ($testMode) {
            $result = 'true';
        } else {
            $m = new \Memcached();
            $m->addServer($server, $port);

            /**
             * check connection
             */
            $ver = $m->getVersion();
            if ($ver === false) {
                Throw new Exception('can\'t connect to memcached');
            }

            $response = $m->get($key);

            $this->logIt('response', $response);

            if ($response != false) {
                $result = 'true';
                $this->context->setParam($this->params['saveToParam'], $response);
            }
        }
        return (string)$this->node->resultlist[0]->$result;
    }

}

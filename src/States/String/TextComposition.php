<?php


namespace Nashimoari\ScenarioHandler\States;


use Nashimoari\ScenarioHandler\Helpers\Utils;


class TextComposition extends AbstractState
{

    public function run(): string
    {
        $result = '';

        $saveParamPath = (string)$this->node->parameterslist[0]->saveParamPath;
        $textBlocks = $this->node->parameterslist[0]->textBlocks[0];

        foreach ($textBlocks as $param) {
            $type = (string)$param[@type];

            if ($type == 'text') {
                $result .= (string)$param;
                continue;
            }

            if ($type == 'param') {
                $paramName = (string)$param;
                $result .= $this->params[$paramName];
                continue;
            }

            if ($type == 'arrayCycle') {
                $arr = $this->context->getParam((string)$param[@paramName]);
                foreach ($arr as $item) {
                    $result .= Utils::paramsReplacer($item, (string)$param);
                }
                continue;
            }
        }
        $this->context->setParam($saveParamPath, $result);

        return (string)$this->node->nextNode;
    }
}

<?php


namespace Nashimoari\ScenarioHandler\States\Chat2Desk;


use Nashimoari\ScenarioHandler\States\AbstractState;


final class GetClientInfo extends AbstractState
{
    public function run() : string
    {
        $clientId = $this->params['clientId'];

        if (empty($clientId)) {
            return (string)$this->node->resultlist[0]->false;
        }

        $APIKey = $this->params['APIKey'];
        $SaveToParam = $this->params['saveToParam'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.chat2desk.com/v1/clients/{$clientId}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: {$APIKey}"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $responseDetails = curl_getinfo($curl);

        curl_close($curl);

        if ($err) {
            return (string)$this->node->resultlist[0]->false;
        }

        if ($responseDetails['http_code'] !== 200) {
            return (string)$this->node->resultlist[0]->false;
        }

        $this->context->setParam($SaveToParam, json_decode($response, 1));
        $nextNode = (string)$this->node->resultlist[0]->true;

        return $nextNode;
    }
}

<?php


namespace nashimoari\ScenarioHandler\States;


use Nashimoari\ScenarioHandler\Helpers\Utils;


class APICall extends AbstractState
{
    /**
     * @return string
     */
    public function run(): string
    {
        $result = 'false';

        $postData = [];
        $testMode = $this->context->getTestMode();

        $curlOptPost = 0;

        if ($this->params['HTTPMethod'] == 'POST') {
            $curlOptPost = 1;
            $postData = $this->preparePostData();
        }

        $this->logIt('URL', $this->params['URL']);

        /**
         * Check test mode
         */
        if ($testMode) {
            $this->context->setTestData((string)$this->node[@name], ['URL' => $this->params['URL'], 'postData' => $postData]);
            $result = 'true';

        } else {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_POST => $curlOptPost,
                CURLOPT_HEADER => 0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_SSL_VERIFYPEER => 1,
                CURLOPT_URL => $this->params['URL']
            ));

            if ($HTTPMethod = 'POST') {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
            }

            $response = curl_exec($curl);
            curl_close($curl);

            $this->logIt('response', $response);

            if ($response != false) {
                $result = 'true';
            }

        }
        return (string)$this->node->resultlist[0]->$result;
    }


    /**
     * Prepare post data
     *
     */
    private function preparePostData()
    {
        $postArray = [];

        if (isset($this->node->POSTParams[0])) {
            foreach ($this->node->POSTParams[0] as $item) {
                $name = $item->getName();
                $postArray[$name] = Utils::paramsReplacer($this->variables, (string)$item);
            }
        }
        if (isset($this->node->PostArrays[0])) {
            foreach ($this->node->PostArrays[0] as $item) {
                $var = (string)$item;
                $postArray = array_merge($postArray, $this->variables[$var]);
            }
        }

        $this->logIt('postArray', $postArray);
        $postData = http_build_query($postArray);

        return $postData;
    }
}

<?php


namespace Nashimoari\ScenarioHandler\States\Services\Settings;


use Nashimoari\ScenarioHandler\States\AbstractState;
use Nashimoari\ScenarioHandler\Helpers\Utils;

final class Set extends AbstractState
{
    private $data = [];

    public function run(): string
    {
        $settings = new Settings();
        $id = $this->params['id'];
        $code = $this->params['code'];

        try {
            $val = $settings->get($code, $id);
            $this->data = json_decode($val, true);
        } catch (\Exception $e) {

        }

        $this->prepareSavedData();

        $this->logIt('resArray', $this->data);

        $settings->update('ImBotCache', $id, json_encode($this->data));

        return (string)$this->node->nextNode;
    }


    /**
     * Prepare post data
     *
     */
    private function prepareSavedData()
    {
        if (isset($this->node->settingsData[0])) {
            foreach ($this->node->settingsData[0] as $item) {
                $name = $item->getName();
                $this->data[$name] = Utils::paramsReplacer($this->variables, (string)$item);
            }
        }
    }

    /**
     * load already saved data
     **/
    private function loadData($path)
    {

        if (file_exists($path)) {
            $string = file_get_contents($path);
            $this->data = json_decode($string, true);
        }

    }
}

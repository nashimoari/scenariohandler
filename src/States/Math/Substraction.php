<?php


namespace Nashimoari\ScenarioHandler\States\Math;

use Nashimoari\ScenarioHandler\States\AbstractState;
final class Substraction extends AbstractState
{

    public function run(): string
    {
        try {
            $nextNode = 'true';

            $minuend = (int)$this->params['minuend'];
            $this->logIt('minuend', $minuend);

            $subtrahend = (int)$this->params['subtrahend'];
            $this->logIt('subtrahend', $subtrahend);

            $difference = $minuend - $subtrahend;
            $this->logIt('difference', $difference);

            $this->context->setParam($this->params['saveTo'], $difference);
        } catch (\Throwable $e) {
            $nextNode = 'false';
        }

        return (string)$this->node->resultlist[0]->$nextNode;
    }
}

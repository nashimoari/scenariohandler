<?php


namespace Nashimoari\ScenarioHandler\States\Services\Laravel;


use Modules\Core\Entities\Mail;
use Nashimoari\ScenarioHandler\States\AbstractState;

class EmailSend extends AbstractState
{

    public function run(): string
    {
        $nextNode = 'false';
        $now = new \DateTime();

        $mailData = [
            'status' => 0,
            'sender' => $this->params['From'],
            'recipient' => $this->params['To'],
            'subject' => $this->params['Subject'],
            'message' => $this->params['Body'],
            'headers' => '',
            'create_dt' => $now,
            'send_dt' => $now,
            'direction' => $this->params['Direction'],
            'driver' => 'Wave_Mail_Driver_SwiftMailer'
        ];

        $this->logIt('mailData', $mailData);

        // For TestMode
        if ($this->context->getTestMode() == 1) {
            return (string)$this->node->resultlist->true;
        }

        if (Mail::create($mailData)) {
            $nextNode = 'true';
        }

        return (string)$this->node->resultlist->$nextNode;


    }
}

<?php


namespace Nashimoari\ScenarioHandler\States;


use Nashimoari\ScenarioHandler\Context;

class SubScenario extends AbstractState
{

    public function run()
    {
        $testMode = $this->params['testMode'];
        $debugMode = $this->params['debugMode'];
        $scenarioName = $this->params['scenarioName'];

        $context = new Context($this, $testMode);
        if ($debugMode == 1) {
            $context->setDebugModeOn();
        }

        $context->setRequest($this->context->getRequest());
        $context->setParams($this->context->getParams());
        $context->run($scenarioName);

        return (string)$this->node->nextnode;
    }
}

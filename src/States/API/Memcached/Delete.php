<?php


namespace Nashimoari\ScenarioHandler\States\Memcached;


use Nashimoari\ScenarioHandler\States\AbstractState;

final class Delete extends AbstractState
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run():string
    {
        $result = 'false';
        $testMode = $this->context->getTestMode();
        $key = $this->params['Key'];
        if (isset($this->params['TTL'])) {
            $ttl = $this->params['TTL'];
        } else {
            $ttl = 0;
        }

        $server = $this->context->getGlobalVariable('memcachedServer');
        $port = $this->context->getGlobalVariable('memcachedPort');


        /**
         * Check test mode
         */
        if ($testMode) {
            $this->logIt('key', $key);
            $this->logIt('ttl', $ttl);

            $result = 'true';
        } else {
            $m = new \Memcached();
            $m->addServer($server, $port);

            /**
             * check connection
             */
            $ver = $m->getVersion();
            if ($ver === false) {
                Throw new \Exception('can\'t connect to memcached');
            }

            $response = $m->delete($key, $ttl);

            $this->logIt('response', $response);

            if ($response != false) {
                $result = 'true';
            }

        }
        return (string)$this->node->resultlist[0]->$result;
    }

}

<?php

namespace Nashimoari\ScenarioHandler;

use Nashimoari\ScenarioHandler\States\AbstractState;

final class Context
{
    private $isEnd = false;
    private $currentNodeName;
    private $nodeCnt = 0;
    private $xml;
    private $node;

    /**
     * @var AbstractState
     */
    private $state;
    private $parent;
    private $request;
    private $params = [];
    private $testData = [];
    private $testMode;
    private $debugMode = false;
    private $log = [];
    private $globalVariables = [];

    /**
     * @var bool Статус завершения сценария (true - все нормально, false - во время выполнения произошла ошибка)
     */
    private $finishStatus = true;

    /**
     * @var int Максимальное количество повторов одной и той же ноды
     * 0 - это значит что повторы не допускаются
     * 1 - это значит что нода может быть выполнена дважды ... и т.д.
     */
    private $maxRetryNodeCounter = 0;

    /**
     * @var array Перечень названий нод по которым уже прошлись и количество проходов
     */
    private $nodeListProcessed = [];

    /**
     * @var string Path to scenarios dir
     */
    private $scenarioPath;

    public function __construct($parent, $testMode = 0)
    {
        $this->parent = $parent;
        $this->testMode = $testMode;
    }

    public function getDebugMode()
    {
        return $this->debugMode;
    }

    public function setDebugModeOn()
    {
        $this->debugMode = true;
    }

    public function setDebugModeOff()
    {
        $this->debugMode = false;
    }


    public function getTestMode()
    {
        return $this->testMode;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setRequest($request)
    {
        $this->request = $request;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function setParam($index, $val)
    {
        $this->params[$index] = $val;
    }

    public function setParams($data)
    {
        $this->params = $data;
    }

    public function getParam($index)
    {
        return $this->params[$index];
    }

    public function getParams()
    {
        return $this->params;
    }

    public function setTestData($nodeName, $val)
    {
        $this->testData[$nodeName] = $val;
    }

    public function getTestData()
    {
        return $this->testData;
    }

    public function getGlobalVariable($name)
    {
        return $this->globalVariables[$name];
    }

    public function setFinishStatus($status)
    {
        $this->finishStatus = $status;
    }

    public function setScenarioPath($scenarioPath)
    {
        $this->scenarioPath = $scenarioPath;
    }


    private function initGlobalVariables()
    {
        if (!isset($this->xml->variables[0])) {
            return;
        }

        foreach ($this->xml->variables[0] as $key => $item) {
            $this->globalVariables[$key] = (string)$item;
        }

        /**
         * Подтягиваем отладочный режим из глобальных переменных
         */
        if (isset($this->globalVariables['debugMode']) && $this->globalVariables['debugMode'] === '1') {
            $this->setDebugModeOn();
        }

        /**
         * Подтягиваем параметр максимального количества повторов из глобальных переменных
         */
        if (isset($this->globalVariables['maxRetryNodeCounter'])) {
            $this->maxRetryNodeCounter = $this->globalVariables['maxRetryNodeCounter'];
        }

    }

    public function getTextLog()
    {
        $textLog = '';
        $i = 1;

        if (isset($this->log['ErrorData'])) {
            $textLog .= 'Error data:' . PHP_EOL;
            $textLog .= $this->log['ErrorData']['message'] . PHP_EOL;
            $textLog .= $this->log['ErrorData']['Trace'] . PHP_EOL;
        }

        foreach ($this->log['automat'] as $item) {
            $textLog .= "==================================" . PHP_EOL;
            $textLog .= "Step {$i}: {$item['nodeName']} ->  {$item['nextNodeName']} " . PHP_EOL;
            $textLog .= print_r($item['data'], 1);
            $i++;
        }

        return $textLog;
    }

    public function getArrayLog()
    {
        return $this->log;
    }

    public function getInputRules($scenarioName)
    {
        try {
            $this->loadScenario($scenarioName);
            $result = $this->xml->xpath('//inputRules')[0];
        } catch (\Throwable $e) {
            $result = false;
        }
        return $result;
    }

    public function run($scenarioName)
    {
        try {
            $this->loadScenario($scenarioName);
            $this->transitionToByNodeCode($this->currentNodeName);

            while (!$this->isEnd) {
                $nextNodeName = $this->state->run();

                $this->log['automat'][] = ['nodeName' => $this->currentNodeName, 'nextNodeName' => $nextNodeName, 'data' => $this->state->getLog()];

                $this->transitionToByNodeCode($nextNodeName);
            }
        } catch (\Throwable $e) {
            $this->finishStatus = false;
            $this->log['ErrorData'] = ['message' => $e->getMessage(), 'Trace' => $e->getTraceAsString()];
        }

        return $this->finishStatus;
    }

    public function runXML($xml)
    {
        $this->xml = $xml;
        $this->initGlobalVariables();

        $this->transitionToByNodeCode($this->currentNodeName);

        while (!$this->isEnd) {
            $nextNodeName = $this->state->run();

            $this->log['automat'][] = ['nodeName' => $this->currentNodeName, 'nextNodeName' => $nextNodeName, 'data' => $this->state->getLog()];

            $this->transitionToByNodeCode($nextNodeName);
        }
    }

    public function exportScenarioToJointJSON($scenarioName)
    {
        $this->loadScenario($scenarioName);

        $out = [];
        foreach ($this->xml->automat as $item) {
            $node = [];
            $node['type'] = "uml.State";
            $node['name'] = (string)$item[@name];

            //$transition

        }
    }

    private function loadScenario($scenarioName)
    {
        if (!isset($this->scenarioPath)) {
            throw new \Exception('Set scenarioPath');
        }

        $scenarioPath = storage_path() . "/app/Scenarios/{$scenarioName}.xml";
        $this->xml = simplexml_load_file($scenarioPath);
        if (!$this->xml) {
            throw new \Exception('No such file');
        }

        $this->initGlobalVariables();
    }

    private function transitionToByNodeCode($nextNodeName)
    {
        if ((strlen($nextNodeName) == 0) && ($this->nodeCnt > 0)) {
            $this->isEnd = true;
            return true;
        }

        if (!isset($nextNodeName)) {
            $nextNodeName = (string)$this->xml->automat->node[0][@name];
        }
        $this->currentNodeName = $nextNodeName;

        /**
         * Check for repetition count
         */
        if (isset($this->nodeListProcessed[$nextNodeName])) {
            $this->nodeListProcessed[$nextNodeName] += 1;
        } else {
            $this->nodeListProcessed[$nextNodeName] = 1;
        }

        if ($this->nodeListProcessed[$nextNodeName] > $this->maxRetryNodeCounter + 1) {
            $this->isEnd = true;
            return false;
        }


        $node = $this->xml->xpath('//automat/node[@name=\'' . $nextNodeName . '\']');
        $object = (string)$node[0][@object];

        $stateClass = "Nashimoari\\ScenarioHandler\\States\\{$object}";

        $this->state = new $stateClass($this, $node[0]);

        /** check state class if implemented of AbstractState */
        if (!is_subclass_of($this->state, 'Nashimoari\ScenarioHandler\States\AbstractState')) {
            throw new \Exception("State class {$object} is not extend AbstractState");
        }

        $this->node = $node[0];

        $this->nodeCnt += 1;
        return true;
    }
}

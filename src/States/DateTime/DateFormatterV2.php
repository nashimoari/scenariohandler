<?php

namespace Nashimoari\ScenarioHandler\States\DateTime;

use DateTimeZone;
use Nashimoari\ScenarioHandler\States\AbstractState;


/**
 * На входе
 * 1) дата и время в одном параметре
 * 2) формат входной даты
 * 3) формат выходной даты
 * На выходе
 * Дата в требуемом формате
 * Class DateFormatterV2
 * @package App\Services\ScenarioProcessor\States\DateTime
 */
final class DateFormatterV2 extends AbstractState
{
    private $arrDays = [
        'RU' => ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
        'EN' => ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
    ];


    public function run() :string
    {
        /**
         * Получаем день недели
         *
         */

        $nextNode = 'false';
        try {

            $dateTime = $this->params['dateTime'];
            $inputFormat = $this->params['inputFormat'];

            /**
             * Тайм зона в которое нужно перевести
             */
            $toTimeZone = (string)$this->params['toTimeZone'];
            $toTimeZone = $this->prepareTimeZone($toTimeZone);
            $this->logIt('preparedToTimeZone', $toTimeZone);


            $dateTime = \DateTime::createFromFormat($inputFormat, $dateTime);
            if ($dateTime === false) {
                $this->logIt("createFromFormat {$inputFormat}", 'error');
                throw new \Exception('DateTime\DateTimePrepare: createFromFormat error');
            }

            $dateTime->setTimezone(new DateTimeZone($toTimeZone));
            $formattedDT = $dateTime->format($this->params['format']);
            $formattedDT = $this->weekDayToRUS($formattedDT);
            $this->logIt('formattedDT', $formattedDT);

            $this->context->setParam($this->params['saveTo'], $formattedDT);
            $nextNode = 'true';
        } catch (\Throwable $e) {
            $nextNode = 'false';
        }

        return (string)$this->node->resultlist[0]->$nextNode;
    }


    private function weekDayToRUS($text)
    {
        return str_replace($this->arrDays['EN'], $this->arrDays['RU'], $text);
    }

    private function prepareTimeZone($tzHour)
    {
        /**
         * Проверяем что у нас не отрицательное значение, так как если положительное, то нам нужно добавить +
         *
         */
        if ($tzHour < 0) {
            if (strlen($tzHour) == 2) {
                $timeZone = '-0' . substr($tzHour, 1, 1);
            }
        } else {
            if (strlen($tzHour) == 1) {
                $timeZone = '+0' . $tzHour;
            } else {
                $timeZone = '+' . $tzHour;
            }
        }

        /**
         * Добавляем в конец два нуля чтобы получилось корректное значение смещения часового пояса
         */
        $timeZone = $timeZone . '00';
        return $timeZone;
    }
}
